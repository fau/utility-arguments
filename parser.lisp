;;;; parser.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(defgeneric parse-option (option designator plist utility-arguments argument))


(defmethod parse-option ((self option-flag) designator plist utility-arguments argument)
  (declare (ignore designator))

  (with-slots (key) self
    (incf (getf plist key 0)))
  (values plist utility-arguments argument))


(defmethod parse-option ((self option-with-argument) designator plist utility-arguments argument)
  (with-slots (key) self
    (setf argument
          (or argument
              (trim-space (pop utility-arguments))))

    (when (not argument)
      (missing-option-argument designator))

    (setf (getf plist key)
          (parse-option-argument argument self designator)))
  (values plist utility-arguments))


(defmethod parse-option ((self option-with-optional-argument) designator plist utility-arguments argument)
  (with-slots (key) self
    (setf (getf plist key)
          (when argument
            (parse-option-argument argument self designator))))
  (values plist utility-arguments))


(defparameter *separator* #\,
  "Character that separates the arguments in a sequence.
Setting it to nil, disables separation.")
(export '*separator*)


(defun csv (string)
  (when (blank? *separator*)
    (setf string (normalize-space string)))
  (mapcar #'trim-space (separate string *separator*)))


(defmethod parse-option ((self option-with-sequence) designator plist utility-arguments argument)
  (with-slots (key) self
    (setf argument
          (csv (or argument
                   (pop utility-arguments))))

    (when (not argument)
      (missing-option-argument designator))

    (mapl (lambda (l)
            (setf (car l) (parse-option-argument (car l) self designator)))
          argument)

    (setf (getf plist key) (nconc (getf plist key) argument)))
  (values plist utility-arguments))


(defmethod parse-option ((self option-with-optional-sequence) designator plist utility-arguments argument)
  (with-slots (key) self
    (setf argument
          (mapl (lambda (l)
                  (setf (car l)
                        (when (car l)
                          (parse-option-argument (car l) self designator))))
                (csv argument)))

    (setf (getf plist key) (nconc (getf plist key) argument)))
  (values plist utility-arguments))


(defun short-option? (string)
  (when (> (length string) 1)
    (and (char=  (char string 0) #\-)
         (char/= (char string 1) #\-)
         (not (blank? (char string 1))))))


(defun parse-short-spec (spec utility-arguments options plist)
  (do ((p 1))
      ((let ((c (char spec p)))
         (when (blank? c)
           (syntax-error spec))

         (let ((a (trim-space (subseq spec (incf p)))))
           (multiple-value-setq (plist utility-arguments a)
             (parse-option (find-option c options) c
                           plist utility-arguments a))
           (not a)))))
  (values plist utility-arguments))


(defun long-option? (string)
  (when (> (length string) 2)
    (and (char= (char string 0) #\-)
         (char= (char string 1) #\-)
         (not (blank? (char string 2))))))


(defun split-long-spec (string)
  (let ((p (position #\= string)))
    (if p
        (values (trim-space (subseq string 0 p))
                (trim-space (subseq string (1+ p)))
                t)
        (progn
          (setf p (position-if #'blank? string))
          (if p
              (values (trim-space (subseq string 0 p))
                      (trim-space (subseq string (1+ p))))
              (values (trim-space (subseq string 0 p))
                      nil))))))


(defun parse-long-spec (spec utility-arguments options plist)
  (multiple-value-bind (s a =?)
      (split-long-spec (subseq spec 2))

    (let ((o (find-option s options)))
      (cond
        (a
         (let (dummy)
           (multiple-value-setq (plist dummy a)
             (parse-option o s plist nil a)))
         (when a
           (excessive-option-arguments s (csv a)))
         (values plist utility-arguments))

        (=?
         (multiple-value-setq (plist)
           (parse-option o s plist nil nil))
         (values plist utility-arguments))

        (t
         (parse-option o s plist utility-arguments nil))))))


(defun end-of-options? (string)
  (and (= (length string) 2)
       (char= (char string 0) #\-)
       (char= (char string 1) #\-)))


(defun parse-utility-arguments (utility-arguments options)
  (let (arguments plist)
    (do ((a (pop utility-arguments)
            (pop utility-arguments)))
        ((not a))
      (setf a (trim-space a))

      (cond
        ((short-option? a)
         (multiple-value-setq (plist utility-arguments)
           (parse-short-spec a utility-arguments
                             options plist)))

        ((long-option? a)
         (multiple-value-setq (plist utility-arguments)
           (parse-long-spec a utility-arguments
                            options plist)))

        ((end-of-options? a)
         (dolist (o utility-arguments)
           (push o arguments))
         (setf utility-arguments nil))

        (t
         (push a arguments))))
    (values (nreverse arguments) plist)))


;;;; parser.lisp ends here
