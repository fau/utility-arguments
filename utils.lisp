;;;; utils.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(defun blank? (char)
  (find char '(#\Space #\Tab #\Linefeed #\Return)))


(defun trim-space (string)
  (let ((s (position-if-not #'blank? string)))
    (when s
      (let ((e (position-if-not #'blank? string :start s :from-end t)))
        (if e
            (subseq string s (1+ e))
            (subseq string s))))))


(defun normalize-space (string)
  (labels ((next (pos acc)
             (if pos
                 (let ((s (position-if-not #'blank? string :start pos)))
                   (if s
                       (let ((e (position-if #'blank? string :start s)))
                         (next e (concatenate 'string acc (when acc (string #\Space))
                                              (subseq string s e))))
                       acc))
                 acc)))
    (next 0 (list))))


(defun split-at (sequence position)
  (values (subseq sequence 0 position)
          (subseq sequence position)))


(defun separate (sequence separator)
  (labels ((next (seq acc)
             (let ((p (position separator seq)))
               (if p
                   (next (subseq seq (1+ p)) (cons (subseq seq 0 p) acc))
                   (nreverse (cons seq acc))))))
    (when sequence
      (next sequence (list)))))


(defun plist-keys-if (predicate plist)
  (do (l)
      ((null plist)
       (nreverse l))
    (when (funcall predicate (cadr plist))
      (push (car plist) l))
    (setf plist (cddr plist))))


(defun plist-keys (plist)
  (do (l)
      ((null plist)
       (nreverse l))
    (push (car plist) l)
    (setf plist (cddr plist))))


(defun lambda-variables (lambda-list)
  (mapcan (lambda (e)
            (when (not (find e lambda-list-keywords))
              (list (if (atom e)
                        e
                        (if (atom (car e))
                            (car e)
                            (cadar e))))))
          lambda-list))


(defun lambda-key-parameters (lambda-list)
  (let ((s (position '&key lambda-list)))
    (when s
      (mapcan (lambda (p)
                (if (atom p)
                    (list (make-keyword p) nil)

                    (if (atom (car p))
                        (list (make-keyword (car p)) (cadr p))

                        (if (cdar p)
                            (list (caar p) (cadr p))
                            (list (make-keyword (caar p)) (cadr p))))))

              (subseq lambda-list (1+ s)
                      (position-if (lambda (key)
                                     (or (eq key '&allow-other-keys)
                                         (eq key '&aux)))
                                   lambda-list))))))


(defun lambda-arity (lambda-list)
  (do ((a (list 0 0 nil))
       (i 0))
      ((not (case (pop lambda-list)
              ((nil &key &aux))
              (&rest
               (setf (elt a 2) t)
               nil)
              (&optional
               (setf i 1))
              (t
               (incf (elt a i)))))
       (values-list a))))


(defun allow-other-keys-lambda? (lambda-list)
  (when (find '&allow-other-keys lambda-list)
    t))


;;;; utils.lisp ends here
