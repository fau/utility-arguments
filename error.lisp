;;;; error.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(define-condition utility-argument-error (error)
  ((text :initarg :text :reader utility-argument-error-text))

  (:report (lambda (c s)
             (write-string (utility-argument-error-text c) s)))

  (:documentation "Condition signaled for utility argument related errors, such as bad
syntax, unknown options, or type errors."))
(export 'utility-argument-error)


(defun utility-argument-error (control &rest arguments)
  (error 'utility-argument-error
         :text (apply #'format nil control arguments)))


(defun syntax-error (literal)
  (utility-argument-error
   "Syntax error `~a'." literal))


(defun no-such-option (literal)
  (utility-argument-error
   "No such option `~a'." literal))


(defun excessive-option-arguments (designator arguments)
  (utility-argument-error
   "Option ~a does not take the argument~p ~{~a~^, ~}." designator (length arguments) arguments))


(defun missing-option-argument (designator)
  (utility-argument-error
   "Missing argument for option ~a." designator))


(defun invalid-option-argument (designator argument type)
  (utility-argument-error
   "Argument ~a for option ~a is not of type ~(~a~)." argument designator type))


(defun invalid-operand-argument (designator argument type)
  (utility-argument-error
   "Argument ~a for operand ~a is not of type ~(~a~)." argument designator type))


(define-condition usage-error (error) ()
  (:documentation "Condition signaled for usage related errors."))
(export 'usage-error)


(define-condition no-such-usage (usage-error)
  ((utility-arguments :initarg :utility-arguments :reader utility-arguments))

  (:report "No such usage.")
  (:documentation "Condition signaled if no suitable usage was found."))
(export '(no-such-usage utility-arguments))


(defun no-such-usage (utility-arguments)
  (error (make-condition 'no-such-usage :utility-arguments utility-arguments)))


(define-condition usage-conflict (usage-error)
  ((usages :initarg :usages :reader conflicting-usages))

  (:report "Conflicting usages.")
  (:documentation "Condition signaled if more than one suitable usage was found."))
(export '(usage-conflict conflicting-usages))


(defun usage-conflict (usages)
  (error (make-condition 'usage-conflict :usages usages)))


;;;; error.lisp ends here
