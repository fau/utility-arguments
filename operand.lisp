;;;; operand.lisp

;;; Copyright (C) 2016, Frank A. U. <fau@riseup.net>
;;;
;;; Permission to use, copy, modify, and/or distribute this software for any
;;; purpose with or without fee is hereby granted, provided that the above
;;; copyright notice and this permission notice appear in all copies.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


(in-package :utility-arguments)


(defclass operand ()
  ((key  :initform nil :initarg :key)
   (type :initform nil :initarg :type)))


(defmethod initialize-instance :after ((self operand) &key key)
  (check-type key (and (not (eql nil)) symbol)))


(defun make-operand (&key key type)
  "Make a new operand.

KEY is a symbol that correlates the operand with a positional
parameter on a usage lambda list.

The parameter TYPE is documented in make-option-with-argument."
  (make-instance 'operand :key key :type type))
(export 'make-operand)


(defun find-operand (key operands)
  "Find operand identifiable by KEY.

KEY is a symbol.  OPERANDS is a list of operand objects."
  (or (find-if (lambda (o)
                 (eq (slot-value o 'key) key))
               operands)
      (error "No such operand ~a." key)))
(export 'find-operand)


(defun parse-operand-argument (argument operand)
  (with-slots (key type) operand
    (if (functionp type)
        (funcall type argument key)

        (case type
          ((nil string) argument)

          (t (multiple-value-bind (obj pos)
                 (handler-case
                     (uiop:safe-read-from-string argument)
                   (error ()
                     (invalid-operand-argument key argument type)))

               (when (or (< pos (length argument)) (not (typep obj type)))
                 (invalid-operand-argument key argument type))
               obj))))))


;;;; operand.lisp ends here
